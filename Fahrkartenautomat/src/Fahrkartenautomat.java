﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0.00; 
       double eingezahlterGesamtbetrag = 0.00;
       double eingeworfeneMünze = 0.00;
       double rückgabebetrag;
       double fahrkarte;
   	   double einzelfahrschein = 2.90;
   	   double tagesfahrschein = 8.60;
   	   double klTagesfahrschein = 23.50;
       double anzahlDerTickets = 0.00;
       
       do {
       fahrkarte = fahrkartenbestellungErfassen();
       

       
       //farkartenanzahl
       anzahlDerTickets = fahrkartenAnzahlAuswählen();
       
       switch((int)fahrkarte) {
   	case 1:
   		zuZahlenderBetrag = (einzelfahrschein * anzahlDerTickets);
   		System.out.println("\n" + "Zu zahlender Betrag:" + (einzelfahrschein * anzahlDerTickets ));
   		break;
   	case 2: 
   		zuZahlenderBetrag = (tagesfahrschein * anzahlDerTickets);
   		System.out.println("\n" + "Zu zahlender Betrag:" + (tagesfahrschein * anzahlDerTickets ));
   		break;
   	case 3: 
   		zuZahlenderBetrag = (klTagesfahrschein * anzahlDerTickets);
   		System.out.println("\n" + "Zu zahlender Betrag:" + (klTagesfahrschein * anzahlDerTickets ));
   		break;
       }
       
       // Geldeinwurf
       // -----------
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, anzahlDerTickets);
      	  

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(rückgabebetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n\n\n");

       }while(true);
    }
    
    public static double fahrkartenbestellungErfassen()
    {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double einzelfahrschein = 2.90;
    	double tagesfahrschein = 8.60;
    	double klTagesfahrschein = 23.50;
    	double anzahlDerTickets = 0.00;
    	int auswahl = 0;
    	
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
    	System.out.println("Einzelfahrschein Regeltarif AB " + einzelfahrschein + "0 Euro " + "(1)");
    	System.out.println("Tageskarte Regeltarif AB " + tagesfahrschein + "0 Euro " + "(2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB " + klTagesfahrschein + "0 Euro " + "(3)");
    	
    	System.out.println("Geben Sie eine Zahl von 1 bis 3 ein, um das Ticket auszuwählen!");
    	System.out.print("Auswahl: ");
    	
    	
    	do {
   		auswahl = tastatur.nextInt();
    	switch(auswahl) {
    	case 1:
    	case 2:
    	case 3:
   			System.out.println("\n" + "Du hast die Fahrkarte " + auswahl + " gewählt");
    		
    		
    		default:
    			System.out.println("Ungültige Eingabe, bitte erneut versuchen!");
    			break;
    	}
   		
    	
    	}while(!(auswahl == 1 || auswahl == 2 || auswahl == 3));
    		
    	return (double)auswahl;
        
    }
    
    static double fahrkartenAnzahlAuswählen() {
    	Scanner tastatur = new Scanner(System.in);
		System.out.print("Anzahl der Tickets: ");
	    double anzahlDerTickets = tastatur.nextDouble();
    	return anzahlDerTickets;
    }
    
    
    static double fahrkartenBezahlen(double zuZahlenderBetrag, double anzahlDerTickets)
    {
    	Scanner tastatur = new Scanner(System.in);
    	double eingeworfeneMünze;
        double eingezahlterGesamtbetrag;
        
        
    	eingezahlterGesamtbetrag = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.println("Noch zu zahlen: " +  (zuZahlenderBetrag - eingezahlterGesamtbetrag) + "0");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    static void fahrkartenAusgeben()
    {
    	
     	System.out.println("\n" + " Fahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		   } 
           catch (InterruptedException e) 
           {
 			e.printStackTrace();
 		   }
        }
        System.out.println("\n\n");
    }
    
    static void rueckgeldAusgeben(double rückgabebetrag)
    {
    	
        if(rückgabebetrag > 0.00)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + "0" + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt: ");

            while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2;
            }
            while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1;
            }
            while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
}

/*
5. Dadurch, dass sowie die Münzen, als auch die Tickets eine Zahl sind,
sollten die Datentypen der beiden Werte identisch sein.

6. Bei der Berechnung "anzahl * einzelpreis" wird im großen und ganzen
die Anzahl der Tickets mit dem Einzelpreis der Tickets Multipliziert.
Es entsteht also ein neuer Wert der durch die Berechnung entstanden ist,
womit man nun weitere Berechnungen durchführen kann.
*/